#include <cmath>
#include <iostream>
using namespace std;

int main() {
	// This program will display a person's grade's statistics.
	//declaration of variable
	float aGrade, tGrade, resultingMark;
	string result, aComputation, tComputation, pointOrPoints;
	bool mark;
	
	//prompt user
	cout << "Greetings! This program will display your \ngrade's statistics based on your input.\n\n";
	cout << "Academic average grade: ";
	cin >> aGrade;
	cout << "Technical average grade: ";
	cin >> tGrade;
	cout << endl;
	
	//calculations
	//Academic Calculation
	result = (aGrade >= 75 && !(aGrade > 100)) ? (aGrade >= 90 && !(aGrade >= 94)) ? "w/honors": (aGrade >= 94 && !(aGrade >= 97)) ? "w/high honors" : (aGrade >= 97) ? "w/highest honors" : "Passed": 
	(aGrade > 100) ? "ERROR! Grade should not pass mark 100.0" : (aGrade < 70) ? "ERROR! Grade should not be below 70.0" : "Failed";
	cout << "Academic remarks: " << result << endl << endl;
	
	//Academic computation to reach marks
	resultingMark = 90 - aGrade;
	
	//to avoid repetition of the condition, I put the answer in a variable.
	mark = (resultingMark <= 0 && !(resultingMark < -10)) ? true : false;
	
	//computer whether below or above mark.
	aComputation = (mark) ? "has achieved mark by a difference of " : (resultingMark < -10) ? "ERROR! Grade should not pass mark 100." : 
	(aGrade < 70 && !(aGrade == 70)) ? "ERROR! Grade should not be below 70." : "requires an additional ";
	
	//change value of resultingMark to display in statistics.
	resultingMark = (mark && !(resultingMark == 0)) ? round(resultingMark * 100) / -100 : (resultingMark < -10) ? 0 : (aGrade < 70) ? 0 : round(resultingMark * 100) / 100;
	
	//changes the word to point, points, or none for proper grammar.
	pointOrPoints = (resultingMark > 1) ? " points" : (resultingMark < -10) ? "" : (resultingMark == 0) ? "" : " point";
	cout << "Points to reach marks: \n";
	cout << "\tw/honors: " << aComputation << resultingMark << pointOrPoints << endl;
	
	resultingMark = 94 - aGrade;
	mark = (resultingMark <= 0 && !(resultingMark < -6)) ? true : false;
	aComputation = (mark) ? "has achieved mark by a difference of " : (resultingMark < -6) ? "ERROR! Grade should not pass mark 100." : 
	(aGrade < 70 && !(aGrade == 70)) ? "ERROR! Grade should not be below 70." : "requires an additional ";
	resultingMark = (mark && !(resultingMark == 0)) ? round(resultingMark * 100) / -100: (resultingMark < -6) ? 0 : (aGrade < 70) ? 0 : round(resultingMark * 100) / 100;
	pointOrPoints = (resultingMark > 1) ? " points" : (resultingMark < -6) ? "" : (resultingMark == 0) ? "" : " point";
	cout << "\tw/high honors: " << aComputation << resultingMark << pointOrPoints << endl;
	
	resultingMark = 97 - aGrade;
	mark = (resultingMark <= 0 && !(resultingMark < -3)) ? true : false;
	aComputation = (mark) ? "has achieved mark by a difference of " : (resultingMark < -3) ? "ERROR! Grade should not pass mark 100." : 
	(aGrade < 70 && !(aGrade == 70)) ? "ERROR! Grade should not be below 70." : "requires an additional ";
	resultingMark = (mark && !(resultingMark == 0)) ? round(resultingMark * 100) / -100: (resultingMark < -3) ? 0 : (aGrade < 70) ? 0 : round(resultingMark * 100) / 100;
	pointOrPoints = (resultingMark > 1) ? " points" : (resultingMark < -3) ? "" : (resultingMark == 0) ? "" : " point";
	cout << "\tw/highest honors: " << aComputation << resultingMark << pointOrPoints << endl << endl;
	
	//Technical Calculation
	result = (tGrade >= 75 && !(tGrade > 100)) ? (tGrade >= 90 && !(tGrade >= 94)) ? "w/honors": (tGrade >= 94 && !(tGrade >= 97)) ? "w/high honors" : (tGrade >= 97) ? "w/highest honors" : "Passed": 
	(tGrade > 100) ? "ERROR! Grade should not pass mark 100.0" : (tGrade < 70) ? "ERROR! Grade should not be below 70.0" : "Failed";
	cout << "Technical remarks: " << result << endl << endl;
	
	//Technical computation to reach marks
	resultingMark = 90 - tGrade;
	mark = (resultingMark <= 0 && !(resultingMark < -10)) ? true : false;
	tComputation = (mark) ? "has achieved mark by a difference of " : (resultingMark < -10) ? "ERROR! Grade should not pass mark 100." : 
	(tGrade < 70 && !(tGrade == 70)) ? "ERROR! Grade should not be below 70." : "requires an additional ";
	resultingMark = (mark && !(resultingMark == 0)) ? round(resultingMark * 100) / -100: (resultingMark < -10) ? 0 : (tGrade < 70) ? 0 : round(resultingMark * 100) / 100;
	pointOrPoints = (resultingMark > 1) ? " points" : (resultingMark < -10) ? "" : (resultingMark == 0) ? "" : " point";
	cout << "Points to reach marks: \n";
	cout << "\tw/honors: " << tComputation << resultingMark << pointOrPoints << endl;
	
	resultingMark = 94 - tGrade;
	mark = (resultingMark <= 0 && !(resultingMark < -6)) ? true : false;
	tComputation = (mark) ? "has achieved mark by a difference of " : (resultingMark < -6) ? "ERROR! Grade should not pass mark 100." : 
	(tGrade < 70 && !(tGrade == 70)) ? "ERROR! Grade should not be below 70." : "requires an additional ";
	resultingMark = (mark && !(resultingMark == 0)) ? round(resultingMark * 100) / -100: (resultingMark < -6) ? 0 : (tGrade < 70) ? 0 : round(resultingMark * 100) / 100;
	pointOrPoints = (resultingMark > 1) ? " points" : (resultingMark < -6) ? "" : (resultingMark == 0) ? "" : " point";
	cout << "\tw/high honors: " << tComputation << resultingMark << pointOrPoints << endl;
	
	resultingMark = 97 - tGrade;
	mark = (resultingMark <= 0 && !(resultingMark < -3)) ? true : false;
	tComputation = (mark) ? "has achieved mark by a difference of " : (resultingMark < -3) ? "ERROR! Grade should not pass mark 100." : 
	(tGrade < 70 && !(tGrade == 70)) ? "ERROR! Grade should not be below 70." : "requires an additional ";
	resultingMark = (mark && !(resultingMark == 0)) ? round(resultingMark * 100) / -100: (resultingMark < -3) ? 0 : (tGrade < 70) ? 0 : round(resultingMark * 100) / 100;
	pointOrPoints = (resultingMark > 1) ? " points" : (resultingMark < -3) ? "" : (resultingMark == 0) ? "" : " point";
	cout << "\tw/highest honors: " << tComputation << resultingMark << pointOrPoints << endl << endl;
	
	//ending message
	cout << "Thank you for making use of this program! =)\n";
	system("pause");

	//Git repository: https://bitbucket.org/parishilario/grading-statistics/src/master/
	return 0;
}
