# Grading Statistics #

This program takes the user's academic and technical grades input and displays information and statistics about it.

### Features ###

* Will display if passed/failed
* Will display points to reach remarks
* Will display points after reaching remarks

### Suggestions ###

* Add average
* Use if-else statments instead of ternary operators

### Comments ###

My only comment is that I should use if-else statements instead of using ternary operators.
Using ternary operators makes the code too convoluted.